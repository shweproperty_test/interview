-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: shweproperty_new
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sp_property`
--

DROP TABLE IF EXISTS `sp_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_property` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `propertytype` varchar(50) NOT NULL,
  `adv_type` varchar(45) NOT NULL,
  `property_name` text,
  `division` varchar(45) NOT NULL,
  `township` varchar(45) NOT NULL,
  `property_price` decimal(20,2) unsigned DEFAULT NULL,
  `price_type` varchar(45) NOT NULL,
  `availability` varchar(100) DEFAULT NULL,
  `property_address` text,
  `p_dimension_length` varchar(45) NOT NULL,
  `p_dimension_width` varchar(45) NOT NULL,
  `p_dimension_sqft` int(11) NOT NULL,
  `p_dimension_acre` varchar(45) NOT NULL,
  `floor` varchar(45) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `aircorn` varchar(45) NOT NULL,
  `ownership` varchar(45) NOT NULL,
  `bedroom` int(1) NOT NULL,
  `bathroom` varchar(45) NOT NULL,
  `special_feature` text,
  `amentites` text,
  `property_detail` text,
  `submit_date` datetime DEFAULT NULL,
  `published` varchar(45) NOT NULL,
  `published_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `agent_id` varchar(45) NOT NULL,
  `source` varchar(45) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_no` varchar(100) NOT NULL,
  `contact_address` text NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `unit_type` varchar(100) DEFAULT NULL,
  `map_lat` varchar(100) DEFAULT NULL,
  `map_long` varchar(100) DEFAULT NULL,
  `show_map` varchar(3) DEFAULT 'no',
  `total_view` bigint(20) NOT NULL DEFAULT '0',
  `feature_listing` varchar(10) NOT NULL DEFAULT 'no',
  `new_homes_id` int(11) NOT NULL,
  `publish_price` int(1) NOT NULL DEFAULT '0',
  `usd` decimal(20,2) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `property_order` int(11) NOT NULL DEFAULT '12' COMMENT 'feature = 3, paid = 6, unpaid = 9, owner = 12',
  `exclusive` varchar(10) NOT NULL DEFAULT 'no',
  `photo` int(1) unsigned zerofill DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `submit_date` (`submit_date`),
  KEY `feature_listing` (`feature_listing`)
) ENGINE=MyISAM AUTO_INCREMENT=151834 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-14  9:42:13
