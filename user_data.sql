/*
-- Query: SELECT account_type,title,company_name,job_title,mobilephone,website,photo,about_me,username,password,email FROM shweproperty_new.users
LIMIT 0, 50

-- Date: 2018-02-15 09:30
*/
INSERT INTO `users` (`account_type`,`title`,`company_name`,`job_title`,`mobilephone`,`website`,`photo`,`about_me`,`username`,`password`,`email`) VALUES ('individual','Ms','ABC','staff',NULL,'','na_agent.png','','grace','$1$9VLpgZCX$KMi5.qn4mS3NfwMdN1nXu/','gracemmw@gmail.com');
INSERT INTO `users` (`account_type`,`title`,`company_name`,`job_title`,`mobilephone`,`website`,`photo`,`about_me`,`username`,`password`,`email`) VALUES ('agency','ေဒၚ','ဇမၺဴေက်ာ္ အိမ္၊ ၿခံ၊ ေၿမ အက်ိဳးေဆာင္','မန္ေနဂ်ာ',NULL,'','na_agent.png','','ေဒၚပြင့္ျဖဴခိုင္','$1$zlLTA.Uw$lOYuVA4Qk.kAKmR8Vu6.S1','zabukyaw.realestate@gmail.com');
INSERT INTO `users` (`account_type`,`title`,`company_name`,`job_title`,`mobilephone`,`website`,`photo`,`about_me`,`username`,`password`,`email`) VALUES ('agency','Mr','MT&K  Enterprise','Manager',NULL,'','na_agent.png','','Win Myint','$1$1tt4azzp$Sasq7KaSKdxBUX0lMloWy1','mtnk@myanmar.com.mm');
 